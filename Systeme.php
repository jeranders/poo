<?php

/**
 * Class Systeme
 *
 */
class Systeme
{
    private $_id_systeme;
    private $_id_valeur_strategique;
    private $_id_criminalite;
    private $_id_economie;
    private $_systeme_nom;
    private $_systeme_description;
    private $_systeme_date_creation;
    private $_systeme_dimension;
    private $_systeme_valide;
    private $_systeme_starmap;
    private $_systeme_video_presentation;

    /**
     * @param $nomSysteme
     * @return mixed
     * Affiche le nom du système
     */
    public function afficheNomSysteme($nomSysteme)
    {
       return $this->_systeme_nom = $nomSysteme;
    }

    /**
     * @return mixed
     */
    public function getSystemeNom()
    {
        return $this->_systeme_nom;
    }

    /**
     * @param $id_systeme
     */
    public function setIdSysteme($id_systeme)
    {
        $this->_id_systeme = $id_systeme;
    }

    /**
     * @return mixed
     */
    public function getIdSysteme()
    {
        return $this->_id_systeme;
    }

    /**
     * @return mixed
     */
    public function getIdCriminalite()
    {
        return $this->_id_criminalite;
    }

    /**
     * @return mixed
     */
    public function getIdEconomie()
    {
        return $this->_id_economie;
    }

    /**
     * @return mixed
     */
    public function getIdValeurStrategique()
    {
        return $this->_id_valeur_strategique;
    }

    /**
     * @return mixed
     */
    public function getSystemeDateCreation()
    {
        return $this->_systeme_date_creation;
    }

    /**
     * @return mixed
     */
    public function getSystemeDescription()
    {
        return $this->_systeme_description;
    }

    /**
     * @return mixed
     */
    public function getSystemeDimension()
    {
        return $this->_systeme_dimension;
    }

    /**
     * @return mixed
     */
    public function getSystemeStarmap()
    {
        return $this->_systeme_starmap;
    }

    /**
     * @return mixed
     */
    public function getSystemeValide()
    {
        return $this->_systeme_valide;
    }

    /**
     * @return mixed
     */
    public function getSystemeVideoPresentation()
    {
        return $this->_systeme_video_presentation;
    }

    /**
     * @param $id_criminalite
     */
    public function setIdCriminalite($id_criminalite)
    {
        $id_criminalite = (int) $id_criminalite;
        if($id_criminalite > 0)
        {
            $this->_id_criminalite = $id_criminalite;
        }
    }

    /**
     * @param $id_economie
     */
    public function setIdEconomie($id_economie)
    {
        $id_economie = (int) $id_economie;
        if($id_economie > 0)
        {
            $this->_id_economie = $id_economie;
        }
    }

    /**
     * @param $id_valeur_strategique
     */
    public function setIdValeurStrategique($id_valeur_strategique)
    {
        $id_valeur_strategique = (int) $id_valeur_strategique;
        if ($id_valeur_strategique > 0 )
        {
            $this->_id_valeur_strategique = $id_valeur_strategique;
        }
    }

    public function setSystemeDateCreation($systeme_date_creation)
    {
        $this->_systeme_date_creation = $systeme_date_creation;
    }

    /**
     * @param $systeme_description
     */
    public function setSystemeDescription($systeme_description)
    {
        $this->_systeme_description = $systeme_description;
    }

    /**
     * @param $systeme_dimension
     */
    public function setSystemeDimension($systeme_dimension)
    {
        $this->_systeme_dimension = $systeme_dimension;
    }

    /**
     * @param $systeme_nom
     */
    public function setSystemeNom($systeme_nom)
    {
        $this->_systeme_nom = $systeme_nom;
    }

    /**
     * @param $systeme_starmap
     */
    public function setSystemeStarmap($systeme_starmap)
    {
        $this->_systeme_starmap = $systeme_starmap;
    }

    /**
     * @param $systeme_valide
     */
    public function setSystemeValide($systeme_valide)
    {
        $this->_systeme_valide = $systeme_valide;
    }

    /**
     * @param $systeme_video_presentation
     */
    public function setSystemeVideoPresentation($systeme_video_presentation)
    {
        $this->_systeme_video_presentation = $systeme_video_presentation;
    }
}