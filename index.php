<?php
//try
//{
//    $bdd = new PDO('mysql:host=localhost;dbname=spn;charset=utf8', 'root', '');
//}
//catch (Exception $e)
//{
//    exit('Erreur : ' . $e->getMessage());
//}
function chargerClasse($classe)
{
    require $classe . '.php'; // On inclut la classe correspondante au paramètre passé.
}

spl_autoload_register('chargerClasse');

$systeme = new Systeme();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                POO
            </a>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="row" id="red">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-danger coupon">
                    <div class="panel-heading" id="head">
                        <div class="panel-title" id="title">
                            <i class="fa fa-github fa-2x"></i>
                            <span class="hidden-xs">Système <?= $systeme->afficheNomSysteme('Stanton') ?></span>
                            <span class="visible-xs">Système <?= $systeme->afficheNomSysteme('Stanton') ?></span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <img src="http://i.imgur.com/e07tg8R.png" class="coupon-img img-rounded">
                        <div class="col-md-12 well well-sm">
                            <div id="business-info">
                                <ul>
                                    <li><span><i class="fa fa-phone"></i> Propiétaire : UEE</span></li>
                                    <li><span><i class="fa fa-map-marker"></i> Valeur stratégique : Mauve</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut autem doloremque dolorum eligendi enim facilis, in ipsum labore magni modi nemo numquam porro quam quia reiciendis rem sequi, soluta voluptates!
                        </div>
                        <div class="col-md-3">
                            <div class="offer text-danger">
                                <span class="number">5</span>
                                <span class="cents"><sup>UA</sup></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <p class="disclosure">Using Genuine Oil Filter and
                                multigrade oil up to vehicle specification. Lube as
                                necessary. Ester Oil or Synthetic available at additional
                                cost. Excludes hazardous waste fee, tax and shop supplies,
                                where applicable. Offer not valid with previous charges or
                                with any other offers or specials. Customer must offer at
                                time of write-up. No cash value.</p>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="coupon-code">
                            Code: GBWO2
                    <span class="print">
                        <a href="#" class="btn btn-link"><i class="fa fa-lg fa-print"></i> Print Coupon</a>
                    </span>
                        </div>
                        <div class="exp">Expires: Sep 30, 2016</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <pre>
                <?php //var_dump($systeme->afficheNomSysteme()); ?>
            </pre>
        </div>
    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>

